using System;

namespace Tennis
{
    public class TennisGame2 : TennisGame
    {

        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: unknown ill collegue / Lorenzo Raimondi
         *  Title: Tennis scoring algorithm
         *  
         *  Descr: The TennisGame1 class is mento to:
         *      - receive scores hit by the two player during a tennis game
         *      - return the textual description of the current scores
         *      
         * The class has two principal methods named WonPoint and GetScores, 
         * derived from the interface TennisGame, that do respectively:
         * 
         *      WonPoint(): receives the name of the player that hit the point
         *          and increment a varaible holding the count for the player
         *          
         *      GetScores(): returns the textual representation of the scores
         *          according to tennis scoring system. 
         *          Uses the local private variables that hold the score values.
         *          It also assigns to P1res and P2res the textual representation 
         *          of the scores of the two players respectively.
         * 
         * ###################################################################### */

        public int P1point = 0;
        public int P2point = 0;
        /* P1res and P2res string variables are defined as public so probably used outside this class. */
        public string P1res = "";
        public string P2res = "";
        /* Define possible Tennis scores below Winning/Advantage cases */
        private string[] scoreDef = { "Love", "Fifteen", "Thirty", "Forty" };


        /* ######################################################################
       * 
       *  Date: 18/06/2015
       *  Author: Lorenzo Raimondi
       *  Title: Initializer
       *  
       *  Descr: The original initializer needed the name of the two players
       *      in input because they were assigned to local variables, but 
       *      they were not used in implementation so I removed them.
       *      Input variables are maintained for compatibility with testing
       *      methods.
       * 
       * ###################################################################### */
        public TennisGame2(string player1Name, string player2Name)
        {

        }



        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: unknown ill collegue / Lorenzo Raimondi
         *  Title: GetScores - returns textual representation of scores
         *  
         *  Descr: According to integer values in P1score and P2core
         *      variables returns textual representation of the scores.
         *      
         *      Textual representation is different according to the absolute 
         *      value of the scores and to their difference. Possible cases are:
         *          - Scores are equal
         *              - Score value < 3
         *              - Score value >= 3
         *          - Both scores are below 4
         *              - Difference == 1
         *              - Difference == 2
         *          - Other cases (at least one score >= 4)
         *          
         *      This implementtion does not only return the textual representation 
         *          of the score, but also assigns to P1res and P2res the 
         *          respective score representation.
         *          
         * 
         * ###################################################################### */
        public string GetScore()
        {
            string score = "";

            if (P1point == P2point)
            {
                if (P1point < 3)
                {
                    score = scoreDef[P1point] + "-All";
                }
                else
                {
                    score = "Deuce";
                }


            }
            else if (P1point < 4 && P2point < 4)
            {
                P1res = scoreDef[P1point];
                P2res = scoreDef[P2point];

                score = P1res + "-" + P2res;
            }
            else
            {
                int scoreGap = P1point - P2point;
                string playerNumber = scoreGap > 0 ? "1" : "2";         // Determines the player number of the player in advantage or winning

                if (Math.Abs(scoreGap).Equals(1))
                {
                    score = "Advantage player" + playerNumber;
                }
                else
                {
                    score = "Win for player" + playerNumber;
                }


            }

            return score;

        }

        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author:
         *  Title: SetP1Score() and SetP2Score() - Note
         *  
         *  Descr: Theese two functions are usless in the present class, but they
         *      might be called from another class since are declared al public.
         *      So alghough never used here they should be kept in code.
         * 
         * ###################################################################### */
        public void SetP1Score(int number)
        {

            for (int i = 0; i < number; i++)
            {
                P1Score();
            }

        }

        public void SetP2Score(int number)
        {

            for (int i = 0; i < number; i++)
            {
                P2Score();
            }

        }

        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author:
         *  Title: P1Score() and P2Score() - Note
         *  
         *  Descr: The implementation of a function to increment the value of 
         *      a class variable (P1point and P2point) suggests that further
         *      implementaton might follow the simple point assignment action.
         * 
         * ###################################################################### */
        public void P1Score()
        {
            P1point++;
        }

        public void P2Score()
        {
            P2point++;
        }

        /* ######################################################################
        * 
        *  Date: before 18/06/2015
        *  Author: unknown ill collegue
        *  Title: WonPoint - score hit assignment
        *  
        *  Descr: Receives the name of the player that hits the point and 
        *      increments his score by 1 point through the function P1Score() and 
         *      P2Score() respectively. Scores are kept in global class 
        *      variables. 
        * 
        * ###################################################################### */
        public void WonPoint(string player)
        {
            if (player == "player1")
                P1Score();
            else
                P2Score();
        }

    }
}

