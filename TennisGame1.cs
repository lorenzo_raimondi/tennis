using System;
using NUnit.Framework;

namespace Tennis
{
    class TennisGame1 : TennisGame
    {
        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: unknown ill collegue / Lorenzo Raimondi
         *  Title: Tennis scoring algorithm
         *  
         *  Descr: The TennisGame1 class is mento to:
         *      - receive scores hit by the two player during a tennis game
         *      - return the textual description of the current scores
         *      
         * The class has two principal methods named WonPoint and GetScores, 
         * derived from the interface TennisGame, that do respectively:
         * 
         *      WonPoint(): receives the name of the player that hit the point
         *          and increment a varaible holding the count for the player
         *          
         *      GetScores(): returns the textual representation of the scores
         *          according to tennis scoring system. 
         *          Uses the local private variables that hold the score values.
         * 
         * ###################################################################### */

        private int player1Score = 0;
        private int player2Score = 0;

        /* Define possible Tennis scores below Winning/Advantage cases */
        string[] scoreDef = { "Love", "Fifteen", "Thirty", "Forty" };

        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: Lorenzo Raimondi
         *  Title: Initializer
         *  
         *  Descr: The original initializer needed the name of the two players
         *      in input because they were assigned to local variables, but 
         *      they were not used in implementation so I removed them.
         *      Input variables are maintained for compatibility with testing
         *      methods.
         * 
         * ###################################################################### */
        public TennisGame1(string player1Name, string player2Name)
        {

        }

        /* ######################################################################
         * 
         *  Date: before 18/06/2015
         *  Author: unknown ill collegue
         *  Title: WonPoint - score hit assignment
         *  
         *  Descr: Receives the name of the player that hits the point and 
         *      increments his score by 1 point. Scores are kept in global class 
         *      variables. 
         *      Points in Tennis game are incremented one by one (...not like in 
         *      basket !)
         * 
         * ###################################################################### */
        public void WonPoint(string playerName)
        {
            if (playerName == "player1")
                player1Score += 1;
            else
                player2Score += 1;
        }


        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: unknown ill collegue / Lorenzo Raimondi
         *  Title: GetScores - returns textual representation of scores
         *  
         *  Descr: According to integer values in player1Score and player2Score
         *      variables returns textual representation of the scores.
         *      
         *      Textual representation is different according to the absolute 
         *      value of the scores and to their difference. Possible cases are:
         *          - Scores are equal (-> getParity_definition())
         *              - Score value < 3
         *              - Score value >= 3
         *          - At least one of the two in >= 4 (-> getWinAdv_definition())
         *              - Difference == 1
         *              - Difference == 2
         *          - Other cases (not equal and neither of them >= 4)
         *          
         *      
         * 
         * ###################################################################### */
        public string GetScore()
        {

            if (player1Score == player2Score)
            {

                return getParity_definition();

            }
            else if (player1Score >= 4 || player2Score >= 4)
            {

                return getWinAdv_definition();

            }

            return scoreDef[player1Score] + "-" + scoreDef[player2Score];

        }

        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: Lorenzo Raimondi
         *  Title: gets Score definition for tie cases
         *  
         *  Descr: Below 3 each score has its own name (see array), above
         *      is always "Deuce"
         * 
         * ###################################################################### */
        private string getParity_definition()
        {

            if (player1Score < 3)
                return scoreDef[player1Score] + "-All";

            return "Deuce";
        }

        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: Lorenzo Raimondi
         *  Title: gets score definition for Winning and Advantage cases
         *  
         *  Descr: Deals cases of :
         *      - at least one score above 4
         *      
         *      If score difference == 1 returns player in "Advantage"
         *      If score difference == 2 returns the "Win" player
         * 
         * ###################################################################### */
        private string getWinAdv_definition()
        {

            int scoreGap = player1Score - player2Score;
            string playerNumber = scoreGap > 0 ? "1" : "2";         // Determines the player number of the player in advantage or winning

            if (Math.Abs(scoreGap).Equals(1))
                return "Advantage player" + playerNumber;

            return "Win for player" + playerNumber;

        }
    }

}

