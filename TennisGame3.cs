using System;

namespace Tennis
{
    public class TennisGame3 : TennisGame
    {
        /* ######################################################################
        * 
        *  Date: 18/06/2015
        *  Author: unknown ill collegue / Lorenzo Raimondi
        *  Title: Tennis scoring algorithm
        *  
        *  Descr: The TennisGame1 class is mento to:
        *      - receive scores hit by the two player during a tennis game
        *      - return the textual description of the current scores
        *      
        * The class has two principal methods named WonPoint and GetScores, 
        * derived from the interface TennisGame, that do respectively:
        * 
        *      WonPoint(): receives the name of the player that hit the point
        *          and increment a varaible holding the count for the player
        *          
        *      GetScores(): returns the textual representation of the scores
        *          according to tennis scoring system. 
        *          Uses the local private variables that hold the score values.
        * 
        * ###################################################################### */

        private int scorePlayer2;
        private int scorePlayer1;

        /* ######################################################################
         * 
         *  Date: 18/06/2015
         *  Author: Lorenzo Raimondi
         *  Title: Initializer
         *  
         *  Descr: The original initializer needed the name of the two players
         *      in input because they were assigned to local variables, but 
         *      they were not used in implementation so I removed them.
         *      Input variables are maintained for compatibility with testing
         *      methods.
         * 
         * ###################################################################### */
        public TennisGame3(string player1Name, string player2Name)
        {

        }


        /* ######################################################################
        * 
        *  Date: 18/06/2015
        *  Author: unknown ill collegue / Lorenzo Raimondi
        *  Title: GetScores - returns textual representation of scores
        *  
        *  Descr: According to integer values in player1Score and player2Score
        *      variables returns textual representation of the scores.
        *      
        *      Textual representation is different according to the absolute 
        *      value of the scores and to their difference. Possible cases are:
        *         - Scores are both below 4 and their sum is below 6.
        *              First condition defines all scores below "Winning or 
        *              Advantage area".
        *              Second condition exclude the case of scorePlayer1 == 3 
        *              && scorePlayer2 == 3 delt below.
        *          - Other cases including:
        *              - Scores are equal -> "Deuce"
        *              - Winning and Adcantage cases
        * 
        * ###################################################################### */
        public string GetScore()
        {

            int scoreGap = scorePlayer1 - scorePlayer2;
            string score;

            if ((scorePlayer1 < 4 && scorePlayer2 < 4) && (scorePlayer1 + scorePlayer2 < 6))
            {

                string[] scoreDefArray = new String[] { "Love", "Fifteen", "Thirty", "Forty" };
                score = scoreDefArray[scorePlayer1];

                return (scoreGap.Equals(0)) ? score + "-All" : score + "-" + scoreDefArray[scorePlayer2];

            }

            /* Equal scores >= 3 */
            if (scoreGap.Equals(0))
                return "Deuce";

            
            /* At least one of the two scores above 4 */
            string playerNumber = scoreGap > 0 ? "1" : "2";
            return (Math.Abs(scoreGap).Equals(1)) ? "Advantage player" + playerNumber : "Win for player" + playerNumber;


        }

        /* ######################################################################
        * 
        *  Date: before 18/06/2015
        *  Author: unknown ill collegue
        *  Title: WonPoint - score hit assignment
        *  
        *  Descr: Receives the name of the player that hits the point and 
        *      increments his score by 1 point. Scores are kept in global class 
        *      variables. 
        *      Points in Tennis game are incremented one by one (...not like in 
        *      basket !)
        * 
        * ###################################################################### */
        public void WonPoint(string playerName)
        {
            if (playerName == "player1")
                this.scorePlayer1 += 1;
            else
                this.scorePlayer2 += 1;
        }

    }
}

